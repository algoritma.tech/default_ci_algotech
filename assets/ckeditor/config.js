/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	   config.filebrowserBrowseUrl = '../module/ckeditor/kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = '../module/ckeditor/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = '../module/ckeditor/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = '../module/ckeditor/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = '../module/ckeditor/kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = '../module/ckeditor/kcfinder/upload.php?type=flash';
};
