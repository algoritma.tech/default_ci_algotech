
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_gallery">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Gallery</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_kontak">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Kontak</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_konten">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Konten</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_news">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>News</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_slidder">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Slidder</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_testimoni">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Testimoni</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_kategori_produk">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Kategori Produk</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_produk">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Produk</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>tbl_profile">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Profile</span></a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>login/logout">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Logout</span></a>
      </li>

      

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <!-- <div class="sidebar-heading">
        Addons
      </div> -->

      <!-- Nav Item - Pages Collapse Menu -->
      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Login</a>
            <a class="collapse-item" href="register.html">Register</a>
            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="404.html">404 Page</a>
            <a class="collapse-item" href="blank.html">Blank Page</a>
          </div>
        </div>
      </li> -->


      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>

    <!-- End of Sidebar