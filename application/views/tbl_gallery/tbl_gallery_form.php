<div class="card-body">
    <h2 style="margin-top:0px">Gallery <?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
       <div class="form-group">
        <label for="varchar">Nama Gallery <?php echo form_error('nama_gallery') ?></label>
        <input type="text" class="form-control" name="nama_gallery" id="nama_gallery" placeholder="Nama Gallery" value="<?php echo $nama_gallery; ?>" />
    </div>
    <div class="form-group">
        <label for="foto">Foto <?php echo form_error('foto') ?></label><br>
        <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>" />
    </div>
    <div class="form-group">
        <label for="desk">Desk <?php echo form_error('desk') ?></label>
        <textarea class="form-control" rows="3" name="desk" id="desk" placeholder="Desk"><?php echo $desk; ?></textarea>
    </div>
	    <!-- <div class="form-group">
            <label for="timestamp">Tgl Input <?php echo form_error('tgl_input') ?></label>
            <input type="text" class="form-control" name="tgl_input" id="tgl_input" placeholder="Tgl Input" value="<?php echo $tgl_input; ?>" />
        </div> -->
        <input type="hidden" name="id_gallery" value="<?php echo $id_gallery; ?>" /> 
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('tbl_gallery') ?>" class="btn btn-default">Cancel</a>
    </form>
</div>