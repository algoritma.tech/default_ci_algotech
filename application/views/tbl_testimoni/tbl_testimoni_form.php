<div class="card-body">
    <h2 style="margin-top:0px">Testimoni <?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
       <div class="form-group">
        <label for="varchar">Nama Testimoni <?php echo form_error('nama_testimoni') ?></label>
        <input type="text" class="form-control" name="nama_testimoni" id="nama_testimoni" placeholder="Nama Testimoni" value="<?php echo $nama_testimoni; ?>" />
    </div>
    <div class="form-group">
        <label for="varchar">Testimoni <?php echo form_error('testimoni') ?></label>
        <input type="text" class="form-control" name="testimoni" id="testimoni" placeholder="Testimoni" value="<?php echo $testimoni; ?>" />
    </div>
    <div class="form-group">
        <label for="foto">Foto <?php echo form_error('foto') ?></label><br>
        <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>" />
    </div>
    <!-- <div class="form-group">
        <label for="timestamp">Tgl Input <?php echo form_error('tgl_input') ?></label>
        <input type="text" class="form-control" name="tgl_input" id="tgl_input" placeholder="Tgl Input" value="<?php echo $tgl_input; ?>" />
    </div> -->
    <input type="hidden" name="id_testimoni" value="<?php echo $id_testimoni; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tbl_testimoni') ?>" class="btn btn-default">Cancel</a>
</form>
</div>