<div class="card-body">
    <h2 style="margin-top:0px">News <?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
     <div class="form-group">
        <label for="varchar">Judul <?php echo form_error('judul') ?></label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
    </div>
    <div class="form-group">
        <label for="varchar">Desk <?php echo form_error('desk') ?></label>
        <input type="text" class="form-control" name="desk" id="desk" placeholder="Desk" value="<?php echo $desk; ?>" />
    </div>
    <div class="form-group">
        <label for="foto">Foto <?php echo form_error('foto') ?></label><br>
        <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>" />
    </div>
    <div class="form-group">
        <label for="konten">Konten <?php echo form_error('konten') ?></label>
        <textarea class="ckeditor" rows="3" name="konten" id="ckeditor" placeholder="Konten"><?php echo $konten; ?></textarea>
    </div>
    <!-- <div class="form-group">
        <label for="timestamp">Tgl Input <?php echo form_error('tgl_input') ?></label>
        <input type="text" class="form-control" name="tgl_input" id="tgl_input" placeholder="Tgl Input" value="<?php echo $tgl_input; ?>" />
    </div> -->
    <input type="hidden" name="id_news" value="<?php echo $id_news; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tbl_news') ?>" class="btn btn-default">Cancel</a>
</form>
</div>