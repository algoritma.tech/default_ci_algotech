
<h2 style="margin-top:0px">File <?php echo $button ?></h2>
<form action="<?php echo $action; ?>" method="post">
    <div class="form-group">
        <label for="nama_file">Nama File <?php echo form_error('nama_file') ?></label>
        <textarea class="form-control" rows="3" name="nama_file" id="nama_file" placeholder="Nama File"><?php echo $nama_file; ?></textarea>
    </div>
    <div class="form-group">
        <label for="date">Tgl Input <?php echo form_error('tgl_input') ?></label>
        <input type="text" class="form-control" name="tgl_input" id="tgl_input" placeholder="Tgl Input" value="<?php echo date('Y-m-d'); ?>" />
    </div>
    <input type="hidden" name="id_file" value="<?php echo $id_file; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tbl_file') ?>" class="btn btn-default">Cancel</a>
</form>