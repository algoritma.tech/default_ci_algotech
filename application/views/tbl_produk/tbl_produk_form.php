<div class="card-body">
    <h2 style="margin-top:0px">Produk <?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
     <div class="form-group">
        <label for="int">Kategori Produk </label>
                <select class="form-control" name="id_kategori_produk" id="id_kategori_produk">
                    <?php foreach ($tbl_kategori_produk_data as $kategori_produk) { ?>
                      <option value="<?php echo $kategori_produk->id_kategori_produk?>" <?php echo $kategori_produk->id_kategori_produk == $id_kategori_produk ? 'selected': '' ?>>
                        <?php echo $kategori_produk->nama_kategori_produk ?>
                      </option>
                    <?php } ?>
              </select>
    </div>
    <div class="form-group">
        <label for="varchar">Nama Produk <?php echo form_error('nama_produk') ?></label>
        <input type="text" class="form-control" name="nama_produk" id="nama_produk" placeholder="Nama Produk" value="<?php echo $nama_produk; ?>" />
    </div>
    <div class="form-group">
        <label for="foto1">Foto1 <?php echo form_error('foto1') ?></label><br>
        <input type="file" name="foto1" id="foto1" value="<?php echo $foto1; ?>" />
    </div>
    <div class="form-group">
        <label for="foto2">Foto2 <?php echo form_error('foto2') ?></label><br>
        <input type="file" name="foto2" id="foto2" value="<?php echo $foto2; ?>" />
    </div>
    <div class="form-group">
        <label for="foto3">Foto3 <?php echo form_error('foto3') ?></label><br>
        <input type="file" name="foto3" id="foto3" value="<?php echo $foto3; ?>" />
    </div>
    <div class="form-group">
        <label for="int">Desk <?php echo form_error('desk') ?></label>
        <input type="text" class="form-control" name="desk" id="desk" placeholder="Desk" value="<?php echo $desk; ?>" />
    </div>
    <div class="form-group">
        <label for="int">Harga <?php echo form_error('harga') ?></label>
        <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga" value="<?php echo $harga; ?>" />
    </div>
    <div class="form-group">
        <label for="varchar">Berat <?php echo form_error('berat') ?></label>
        <input type="text" class="form-control" name="berat" id="berat" placeholder="Berat" value="<?php echo $berat; ?>" />
    </div>
    <div class="form-group">
        <label for="varchar">Task <?php echo form_error('task') ?></label>
        <input type="text" class="form-control" name="task" id="task" placeholder="Task" value="<?php echo $task; ?>" />
    </div>
    <!-- <div class="form-group">
        <label for="timestamp">Tgl Input <?php echo form_error('tgl_input') ?></label>
        <input type="text" class="form-control" name="tgl_input" id="tgl_input" placeholder="Tgl Input" value="<?php echo $tgl_input; ?>" />
    </div> -->
    <input type="hidden" name="id_produk" value="<?php echo $id_produk; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tbl_produk') ?>" class="btn btn-default">Cancel</a>
</form>
</div>