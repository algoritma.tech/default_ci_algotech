<div class="card-body">
        <h2 style="margin-top:0px">Slidder <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Nama <?php echo form_error('nama') ?></label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
        </div>
	    <div class="form-group">
            <label for="desk">Desk <?php echo form_error('desk') ?></label>
            <textarea class="form-control" rows="3" name="desk" id="desk" placeholder="Desk"><?php echo $desk; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="foto">Foto <?php echo form_error('foto') ?></label><br>
        <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="timestamp">Tgl Input <?php echo form_error('tgl_input') ?></label>
            <input type="text" class="form-control" name="tgl_input" id="tgl_input" placeholder="Tgl Input" value="<?php echo $tgl_input; ?>" />
        </div> -->
	    <input type="hidden" name="id_slidder" value="<?php echo $id_slidder; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('tbl_slidder') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div>