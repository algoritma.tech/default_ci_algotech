<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_kategori_produk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_kategori_produk_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_kategori_produk/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_kategori_produk/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_kategori_produk/';
            $config['first_url'] = base_url() . 'tbl_kategori_produk/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_kategori_produk_model->total_rows($q);
        $tbl_kategori_produk = $this->Tbl_kategori_produk_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_kategori_produk_data' => $tbl_kategori_produk,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_kategori_produk/tbl_kategori_produk_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_kategori_produk_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_kategori_produk' => $row->id_kategori_produk,
		'nama_kategori_produk' => $row->nama_kategori_produk,
		// 'tgl_input' => $row->tgl_input,
	    );
            $this->load->view('tbl_kategori_produk/tbl_kategori_produk_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_kategori_produk'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_kategori_produk/create_action'),
	    'id_kategori_produk' => set_value('id_kategori_produk'),
	    'nama_kategori_produk' => set_value('nama_kategori_produk'),
	    // 'tgl_input' => set_value('tgl_input'),
	);
        renderPage('tbl_kategori_produk/tbl_kategori_produk_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_kategori_produk' => $this->input->post('nama_kategori_produk',TRUE),
		// 'tgl_input' => $this->input->post('tgl_input',TRUE),
	    );

            $this->Tbl_kategori_produk_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tbl_kategori_produk'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_kategori_produk_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_kategori_produk/update_action'),
		'id_kategori_produk' => set_value('id_kategori_produk', $row->id_kategori_produk),
		'nama_kategori_produk' => set_value('nama_kategori_produk', $row->nama_kategori_produk),
		// 'tgl_input' => set_value('tgl_input', $row->tgl_input),
	    );
            renderPage('tbl_kategori_produk/tbl_kategori_produk_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_kategori_produk'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kategori_produk', TRUE));
        } else {
            $data = array(
		'nama_kategori_produk' => $this->input->post('nama_kategori_produk',TRUE),
		// 'tgl_input' => $this->input->post('tgl_input',TRUE),
	    );

            $this->Tbl_kategori_produk_model->update($this->input->post('id_kategori_produk', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_kategori_produk'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_kategori_produk_model->get_by_id($id);

        if ($row) {
            $this->Tbl_kategori_produk_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_kategori_produk'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_kategori_produk'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_kategori_produk', 'nama kategori produk', 'trim|required');
	// $this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

	$this->form_validation->set_rules('id_kategori_produk', 'id_kategori_produk', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tbl_kategori_produk.php */
/* Location: ./application/controllers/Tbl_kategori_produk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-10 06:49:07 */
/* http://harviacode.com */