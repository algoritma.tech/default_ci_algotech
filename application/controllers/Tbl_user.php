<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_user extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_user_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_user/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_user/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_user/index.html';
            $config['first_url'] = base_url() . 'tbl_user/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_user_model->total_rows($q);
        $tbl_user = $this->Tbl_user_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_user_data' => $tbl_user,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_user/tbl_user_list',$data);
      //  $this->load->view('tbl_user/tbl_user_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_user_model->get_by_id($id);
        if ($row) {
            $data = array(
              'iduser' => $row->iduser,
              'email' => $row->email,
              'pass' => $row->pass,
          );
            $this->load->view('tbl_user/tbl_user_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_user'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_user/create_action'),
            'iduser' => set_value('iduser'),
            'email' => set_value('email'),
            'pass' => set_value('pass'),
        );
        $this->load->view('tbl_user/tbl_user_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
              'email' => $this->input->post('email',TRUE),
              'pass' => $this->input->post('pass',TRUE),
          );

            $this->Tbl_user_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tbl_user'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_user_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_user/update_action'),
                'iduser' => set_value('iduser', $row->iduser),
                'email' => set_value('email', $row->email),
                'pass' => set_value('pass', $row->pass),
            );
            $this->load->view('tbl_user/tbl_user_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_user'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('iduser', TRUE));
        } else {
            $data = array(
              'email' => $this->input->post('email',TRUE),
              'pass' => $this->input->post('pass',TRUE),
          );

            $this->Tbl_user_model->update($this->input->post('iduser', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_user'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_user_model->get_by_id($id);

        if ($row) {
            $this->Tbl_user_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_user'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_user'));
        }
    }

    public function _rules() 
    {
       $this->form_validation->set_rules('email', 'email', 'trim|required');
       $this->form_validation->set_rules('pass', 'pass', 'trim|required');

       $this->form_validation->set_rules('iduser', 'iduser', 'trim');
       $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
   }

}

/* End of file Tbl_user.php */
/* Location: ./application/controllers/Tbl_user.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-10 19:06:17 */
/* http://harviacode.com */