<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	function __construct(){
		parent::__construct();		
		$this->load->model('Login_model');

		if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
        
 
	} 
 
	public function index()
	{
		$data = 1;
		renderPage('admin/index',$data);

		//$this->load->view('admin/index.php');
	}
}
